import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Login from './screen/login';

const Stack = createStackNavigator();

export default function App() {
  return (
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName='Login'
          headerMode="none"
        >
          <Stack.Screen
            name="Login"
            component={ Login }
          />
          <Stack.Screen
            name="register"
            component={ register }
          />
        </Stack.Navigator>
      </NavigationContainer>
      );
}
