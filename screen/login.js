import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, Input, Button } from 'react-native-elements';
import { Icon } from 'react-native-vector-icons/FontAwesome';
import auth from '../firebase/auth';

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#c91e60",
        padding: 12,
        alignItems: "center",
        justifyContent: "center"
    },
    header: {
        textAlign: "center",
        fontSize: 45,
        fontWeight: "bold",
        color: "#fff",
        textShadowColor: "#700a31",
        textShadowOffset: {width: 4, height: 3},
        marginBottom: 5
    },
    primary_button: {
        color: "#ffff",
        borderColor: "#fff",
        width: "100%"
    },
    textWhite: {
        color: "#fffff",
        fontSize: 3
    }
});

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: "",
            password: ""
        }
    }
    authentication () {
        auth.signInWithEmailAndPassword(this.state.email, this.state.password)
        .then(res => {
            console.log(res);
        })
    }
    render () {
        return (
            <View style={style.container}>
                <Text style={ style.header }>Sign in</Text>
                <Input
                    name="username"
                    placeholder="email@address.com"
                    label="Your Email Address"
                    style={{ marginTop: 10, color: "white" }}
                    onChange={
                        e => {
                            this.state.email = e.target.value;
                        }
                    }
                />
                <Input
                    name="password"
                    placeholder="Password"
                    label="Password"
                    secureTextEntry={true}
                    style={{ marginTop: 10, color: "#ffff" }}
                    onChange={
                        e => {
                            this.state.password = e.target.value;
                        }
                    }
                />
                <Button
                    title="Signin"
                    type="solid"
                    style={style.primary_button}
                    onPress={
                        () => {
                            this.authentication()
                        }
                    }
                />
                <Button
                    title="Register"
                    type="outline"
                    style={{ marginTop: 10, color: "#fffff" }}
                    onPress={() => {this.props.navigation.navigate('register')}}
                />
            </View>
        );
    }
}

export default Login;