import React, { Component } from 'react-native';
import config from '../firebase/config';
import firebase from 'firebase';

const fb = firebase.initializeApp(config);

export default fb.auth();